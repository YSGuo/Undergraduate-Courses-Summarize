# Undergraduate-Courses-Summarize

[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)

This is a link navigation to some of my undergraduate courses' class summarizes, written in Chinese.

## LICENSE

Copyright (c) 2018 Yang (Simon) Guo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Courses

| [Computer Network A] | [Software Testing and QA] | [IoT Intro] | [Principle of Compiler] |
| :---------: | :---------: | :---------: | :---------: |
| [click](https://github.com/sgyzetrov/Computer-Network-Summarize/blob/master/README.md)|[click](https://github.com/sgyzetrov/Software-Testing-Summarize/blob/master/README.md)|[TBA](https://github.com/sgyzetrov/Computer-Network-Summarize/blob/master/ch3.md)|[TBA](https://github.com/sgyzetrov/Computer-Network-Summarize/blob/master/ch4.md)|
